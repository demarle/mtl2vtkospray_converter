"""
A utililty to convert Wavefront OBJ material files (*.mtl) to
VTK's OSPRay material specification format.

license: Same as VTKs
"""

import sys
import string
import json

allmats = {"family":"OSPRay","version":"0.0","materials":{}}
currentmat = {};

def finisholdmat():
    if currentmat != {}:
      allmats["materials"][currentmat["name"]] = currentmat["content"]
      #print "result:", json.dumps(currentmat)
      #print "------------------"

fname = sys.argv[1]
#print fname

with open(fname) as f:
    content = f.readlines()
content = [x.strip() for x in content]

for l in content:
    if l.startswith("#"):
        #print "ignore ", l
        continue
    if l.startswith("newmtl "):
        finisholdmat();
        currentmat["name"] = l[7:]
        currentmat["content"] = {}
        #print "start ", currentmat["name"]
        continue
    if l.startswith("type "):
        typename = string.capwords(l[5:])
        if typename == "Matte":
            typename = "OBJMaterial"
        currentmat["content"]["type"] = typename
        continue
    if l.startswith("color "):
        vals = [float(i) for i in l[6:].split(" ")]
        if "doubles" not in currentmat["content"]:
            currentmat["content"]["doubles"] = {}
        currentmat["content"]["doubles"]["Kd"] = vals
        continue
    if l.startswith("roughness "):
        vals = [float(i) for i in l[10:].split(" ")]
        if "doubles" not in currentmat["content"]:
            currentmat["content"]["doubles"] = {}
        currentmat["content"]["doubles"]["roughness"] = vals
        continue
    if l.startswith("colorMap "):
        tfilename = l[9:]
        if "textures" not in currentmat["content"]:
            currentmat["content"]["textures"] = {}
        currentmat["content"]["textures"]["map_kd"] = tfilename
        continue
    if l.startswith("bumpMap "):
        tfilename = l[8:]
        if "textures" not in currentmat["content"]:
            currentmat["content"]["textures"] = {}
        currentmat["content"]["textures"]["map_bump"] = tfilename
        continue
    #print "ignoreing ", l

finisholdmat()
print json.dumps(allmats)
